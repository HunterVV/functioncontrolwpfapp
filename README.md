# Function Control
This is small WPF-project for visualizing piecewise linear functions.

## Description
Several functions can be used in the same time. Function knots can be set in the table, wich also support clipboarding data from/to excel.
Save/load of function set supported.

This project is a test task for [POSPEX](http://simmakers.ru/).

## Screenshots
![](https://i.ibb.co/mv6hCpN/Function-Control.png)