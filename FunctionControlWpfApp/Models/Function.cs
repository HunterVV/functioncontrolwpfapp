﻿using FunctionControlWpfApp.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using LiveCharts.Helpers;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace FunctionControlWpfApp.Models
{
    public class Function : INotifyPropertyChanged, IEquatable<Function>, ICloneable
    {

        private const string DefaultFunctionName = "Function";

        [DataMember]
        private string name;
        public string Name 
        { 
            get { return name; } 
            set 
            { 
                name = value;
                OnPropertyChanged();
            }
        }
        public RangeObservableCollection<Point2D> Points { get; set; }

        public Function() : this(DefaultFunctionName)
        {
        }

        public Function(string name)
        {
            this.name = name;
            Points = new RangeObservableCollection<Point2D>();
        }

        public Function(string name, IEnumerable<Point2D> points)
        {
            this.name = name;
            Points = new RangeObservableCollection<Point2D>(points);
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        #region IEquatable<Function> implementation
        public bool Equals(Function other)
        {
            if (other == null)
                return false;

            return name == other.name && Enumerable.SequenceEqual(Points, other.Points);
        }
        #endregion

        #region ICloneable implementation
        public object Clone()
        {
            return new Function(name, Points.Select(p => p.Clone() as Point2D));
        }
        #endregion
    }
}
