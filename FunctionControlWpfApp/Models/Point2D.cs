﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Text;

namespace FunctionControlWpfApp.Models
{
    public class Point2D : INotifyPropertyChanged, IEquatable<Point2D>, ICloneable
    {
        private double x;
        private double y;

        public double X 
        {
            get { return x; } 
            set
            {
                x = value;
                OnPropertyChanged();
            }
        }
        public double Y
        {
            get { return y; }
            set
            {
                y = value;
                OnPropertyChanged();
            }
        }

        public Point2D() {}

        public Point2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        #region IEquatable<Point2D> implementation
        public bool Equals(Point2D other)
        {
            if (other == null)
                return false;

            return x == other.x && y == other.y;
        }
        #endregion

        #region ICloneable implementation
        public object Clone()
        {
            return new Point2D(x, y);
        }
        #endregion
    }
}
