﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Linq;
using System.Windows.Input;
using System.Windows;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization.Json;
using Microsoft.Win32;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Defaults;
using FunctionControlWpfApp.Models;
using FunctionControlWpfApp.Commands;
using System.Collections.Specialized;

namespace FunctionControlWpfApp.ViewModels
{
    class FunctionControlViewModel : INotifyPropertyChanged
    {
        private Function selectedFunction;
        private ObservableCollection<Function> functions;
        private bool inversedFunction = false;

        private string savefileName;
        private ObservableCollection<Function> savedFunctions;

        public Function SelectedFunction
        {
            get { return selectedFunction; }
            set
            {
                if (selectedFunction != null)
                {
                    selectedFunction.Points.CollectionChanged -= OnPointsChange;
                    foreach (var point in selectedFunction.Points)
                        point.PropertyChanged -= OnPointChange;
                }

                selectedFunction = value;

                if (selectedFunction != null)
                {
                    selectedFunction.Points.CollectionChanged += OnPointsChange;
                    foreach (var point in selectedFunction.Points)
                        point.PropertyChanged += OnPointChange;
                }

                OnPropertyChanged();
            }
        }
        public ObservableCollection<Function> Functions 
        {
            get { return functions; } 
            set
            {
                functions = value;
                SelectedFunction = null;
                OnPropertyChanged();
            }
        }
        public bool InversedFunction
        {
            get { return inversedFunction; }
            set
            {
                inversedFunction = value;
                OnPropertyChanged();
            }
        }
        public SeriesCollection SeriesCollection { get; set; }

        public ActionCommand SaveCommand { get; set; }
        public ActionCommand SaveAsCommand { get; set; }
        public ActionCommand LoadCommand { get; set; }
        public ActionCommand AddFunctionCommand { get; private set; }
        public ActionCommand DeleteFunctionCommand { get; private set; }
        public ActionCommand CopyCommand { get; private set; }
        public ActionCommand PasteCommand { get; private set; }

        public FunctionControlViewModel()
        {
            InitiateCommands();

            functions = new ObservableCollection<Function>();
            SeriesCollection = new SeriesCollection();
        }

        private void InitiateCommands()
        {
            SaveCommand = new ActionCommand(SaveCommandExecute, CanSave);
            SaveAsCommand = new ActionCommand(SaveAsCommandExecute, CanSaveAs);
            LoadCommand = new ActionCommand(LoadCommandExecute);
            AddFunctionCommand = new ActionCommand(AddFunctionCommandExecute);
            DeleteFunctionCommand = new ActionCommand(DeleteFunctionCommandExecute, IsFunctionSelected);
            CopyCommand = new ActionCommand(CopyCommandExecute, IsFunctionSelected);
            PasteCommand = new ActionCommand(PasteCommandExecute, IsFunctionSelected);
        }

        private void OnPointsChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var obj in e.NewItems)
                    (obj as Point2D).PropertyChanged += OnPointChange;
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var obj in e.OldItems)
                    (obj as Point2D).PropertyChanged -= OnPointChange;
            }

            UpdateSeriesCollection();
        }

        private void OnPointChange(object sender, PropertyChangedEventArgs e)
        {
            UpdateSeriesCollection();
        }

        private void UpdateSeriesCollection()
        {
            SeriesCollection.Clear();
            if (SelectedFunction != null)
            {
                SeriesCollection.Add(inversedFunction ?
                    new VerticalLineSeries()
                    {
                        Title = SelectedFunction.Name,
                        Values = new ChartValues<ObservablePoint>(SelectedFunction.Points.Select(p => new ObservablePoint(p.Y, p.X))),
                        LineSmoothness = 0
                    } :
                    new LineSeries()
                    {
                        Title = string.Format("{0} Inversed", SelectedFunction.Name),
                        Values = new ChartValues<ObservablePoint>(SelectedFunction.Points.Select(p => new ObservablePoint(p.X, p.Y))),
                        LineSmoothness = 0
                    });
            }
        }

        #region CommandsLogic
        private void SaveCommandExecute(object parameter)
        {
            if (!string.IsNullOrEmpty(savefileName))
            {
                DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(ObservableCollection<Function>));
                using (FileStream stream = new FileStream(savefileName, FileMode.Open))
                {
                    jsonFormatter.WriteObject(stream, Functions);
                }

                savedFunctions = new ObservableCollection<Function>(functions.Select(f => f.Clone() as Function));
            }
            else
                SaveAsCommandExecute(parameter);
        }
        private void SaveAsCommandExecute(object parameter)
        {
            SaveFileDialog dialog = new SaveFileDialog();

            if (dialog.ShowDialog() == true)
            {
                DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(ObservableCollection<Function>));
                using (FileStream stream = new FileStream(dialog.FileName, FileMode.Create))
                {
                    jsonFormatter.WriteObject(stream, Functions);
                }

                savefileName = dialog.FileName;
                savedFunctions = new ObservableCollection<Function>(functions.Select(f => f.Clone() as Function));
            }
        }

        private void LoadCommandExecute(object parameter)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            if (dialog.ShowDialog() == true)
            {
                DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(ObservableCollection<Function>));
                using (FileStream stream = new FileStream(dialog.FileName, FileMode.Open))
                {
                    Functions = jsonFormatter.ReadObject(stream) as ObservableCollection<Function>;
                }

                savefileName = dialog.FileName;
                savedFunctions = new ObservableCollection<Function>(functions.Select(f => f.Clone() as Function));
            }
        }

        private void AddFunctionCommandExecute(object parameter)
        {
            Function func = new Function();
            Functions.Add(func);

            SelectedFunction = func;
        }

        private void DeleteFunctionCommandExecute(object parameter)
        {
            Functions.Remove(parameter as Function);
            UpdateSeriesCollection();
        }

        private void CopyCommandExecute(object parameter)
        {
            Clipboard.SetText(string.Join("\r\n", SelectedFunction.Points.Select(p => string.Format("{0}\t{1}", p.X, p.Y))));
        }

        private void PasteCommandExecute(object parameter)
        {

            List<Point2D> points = new List<Point2D>();
            foreach(var line in Clipboard.GetText().Split("\r\n").Select(line => line.Split('\t')))
            {
                double x, y;
                if (double.TryParse(line[0], out x) && double.TryParse(line[1], out y))
                {
                    points.Add(new Point2D(x, y));
                }
            }
            SelectedFunction.Points.Reset(points);
        }

        private bool IsFunctionSelected(object parameter) => SelectedFunction != null;

        private bool CanSaveAs(object parameter)
        {
            return functions.Count > 0;
        }

        private bool CanSave(object parameter)
        {
            if (!string.IsNullOrEmpty(savefileName))
            {
                return CanSaveAs(parameter) && !Enumerable.SequenceEqual(functions, savedFunctions);
            }

            return CanSaveAs(parameter);
        }
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            if (prop != "Functions")
            {
                UpdateSeriesCollection();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SeriesCollection"));
            }
        }
        #endregion
    }
}
